
# coding: utf-8

# In[ ]:


import torch
import torch.nn as nn
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn.functional as F
import numpy as np


# In[ ]:

def filter_prune(model, pruning_perc):

    masks = []
    shape=[]
    for p in model.parameters():
        if len(p.data.size()) == 4: # nasty way of selecting conv layer
            shape.append(p.size())
    for i in shape:
        tensor=torch.FloatTensor(i).uniform_() > (pruning_perc)/100
        masks.append(tensor.numpy())
#    while current_pruning_perc < pruning_perc:
#        masks = prune_one_filter(model, masks)
    model.set_masks(masks)
    #    current_pruning_perc = prune_rate(model, verbose=False)
    #    print('{:.2f} pruned'.format(current_pruning_perc))

    return masks




def to_var(x, requires_grad=False, volatile=False):
    """
    Varialbe type that automatically choose cpu or cuda
    """
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x, requires_grad=requires_grad, volatile=volatile)


# In[ ]:


def train(model, loss_fn, optimizer, param, loader_train, loader_val=None):

    model.train()
    for epoch in range(param['num_epochs']):
        print('Starting epoch %d / %d' % (epoch + 1, param['num_epochs']))

        for t, (x, y) in enumerate(loader_train):
            x_var, y_var = to_var(x), to_var(y.long())

            scores = model(x_var)
            loss = loss_fn(scores, y_var)

            if (t + 1) % 100 == 0:
                print('t = %d, loss = %.8f' % (t + 1, loss.item()))

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()


# In[ ]:


def test(model, loader):

    model.eval()

    num_correct, num_samples = 0, len(loader.dataset)
    for x, y in loader:
        x_var = to_var(x, volatile=True)
        scores = model(x_var)
        _, preds = scores.data.cpu().max(1)
        num_correct += (preds == y).sum()

    acc = float(num_correct) / num_samples

    print('Test accuracy: {:.2f}% ({}/{})'.format(
        100.*acc,
        num_correct,
        num_samples,
        ))

    return acc



# In[ ]:


def arg_nonzero_min(a):
    """
    nonzero argmin of a non-negative array
    """

    if not a:
        return

    min_ix, min_v = None, None
    # find the starting value (should be nonzero)
    for i, e in enumerate(a):
        if e != 0:
            min_ix = i
            min_v = e
    if not min_ix:
        print('Warning: all zero')
        return np.inf, np.inf

    # search for the smallest nonzero
    for i, e in enumerate(a):
         if e < min_v and e != 0:
            min_v = e
            min_ix = i

    return min_v, min_ix


# In[ ]:


class MaskedConv2d(nn.Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
        super(MaskedConv2d, self).__init__(in_channels, out_channels,
            kernel_size, stride, padding, dilation, groups, bias)
        self.mask_flag = False

    def set_mask(self, mask):
        self.mask = to_var(mask, requires_grad=False)
        self.weight.data = self.weight.data*self.mask.data
        self.mask_flag = True

    def get_mask(self):
        print(self.mask_flag)
        return self.mask

    def forward(self, x):
        if self.mask_flag == True:
            weight = self.weight*self.mask
            return F.conv2d(x, weight, self.bias, self.stride,
                        self.padding, self.dilation, self.groups)
        else:
            return F.conv2d(x, self.weight, self.bias, self.stride,
                        self.padding, self.dilation, self.groups)



# In[8]:


param = {
    'batch_size': 128,
    'test_batch_size': 100,
    'num_epochs': 50,
    'learning_rate': 0.001,
    'weight_decay': 5e-4,
}

train_dataset = datasets.MNIST(root='../mnist/',train=True, download=True,
    transform=transforms.ToTensor())
loader_train = torch.utils.data.DataLoader(train_dataset,
    batch_size=param['batch_size'], shuffle=True)

test_dataset = datasets.MNIST(root='../mnist/', train=False, download=True,
    transform=transforms.ToTensor())
loader_test = torch.utils.data.DataLoader(test_dataset,
    batch_size=param['test_batch_size'], shuffle=True)


# In[ ]:


class ConvNet1(nn.Module):
    def __init__(self):
        super(ConvNet1, self).__init__()

        self.conv1 = MaskedConv2d(1, 6, kernel_size=5, padding=1, stride=1)
        self.relu1 = nn.ReLU(inplace=True)
        self.maxpool1 = nn.MaxPool2d(2)

        self.conv2 = MaskedConv2d(6, 16, kernel_size=5, padding=1, stride=1)
        self.relu2 = nn.ReLU(inplace=True)
        self.maxpool2 = nn.MaxPool2d(2)

        self.linear1 = nn.Linear(16*5*5, 120)
        self.linear2   = nn.Linear(120, 84)
        self.linear3   = nn.Linear(84, 10)


    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.linear1(out))
        out = F.relu(self.linear2(out))
        out = self.linear3(out)
        return out

    def set_masks(self, masks):
        # Should be a less manual way to set masks
        # Leave it for the future
        self.conv1.set_mask(torch.from_numpy(masks[0]).type(torch.FloatTensor))
        self.conv2.set_mask(torch.from_numpy(masks[1]).type(torch.FloatTensor))

net=ConvNet1()


# In[10]:


criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.RMSprop(net.parameters())#, lr=param['learning_rate'],weight_decay=param['weight_decay'])

#train(net, criterion, optimizer, param, loader_train)
#print("##################TEST ACCURACY####################")
#test(net, loader_test)


path = "./cnn_reg_FILTER.pt"
#torch.save(net.state_dict(), path)

#print("#############ACCURACY BEFORE PRUNING##################")
test_model=ConvNet1()
test_model.load_state_dict(torch.load(path, map_location='cpu'))
test(test_model, loader_test)


#print("#############SETTING MASKS##################")
prune_filter={'pruning_perc': 80.}

masks = filter_prune(test_model, prune_filter['pruning_perc'])
test_model.set_masks(masks)
print("--- {}% parameters pruned ---".format(prune_filter['pruning_perc']))

# Check accuracy
print("#############ACCURACY AFTER PRUNING##################")
test(test_model, loader_test)
