import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
from torch.utils.data import sampler


# Train the model
#def train(model, loss_fn, optimizer, param, loader_train, loader_val=None):

#    model.train()
#    for epoch in range(param['num_epochs']):
#        print('Starting epoch %d / %d' % (epoch + 1, param['num_epochs']))
#
#        for t, (x, y) in enumerate(loader_train):
#            x_var, y_var = to_var(x), to_var(y.long())

#            scores = model(x_var)
#            loss = loss_fn(scores, y_var)

#            if (t + 1) % 100 == 0:
#                print('t = %d, loss = %.8f' % (t + 1, loss.data[0]))

#            optimizer.zero_grad()
#            loss.backward()
#            optimizer.step()


# Test the model
# In test phase, we don't need to compute gradients (for memory efficiency)
#def test(model, loader):

#    model.eval()

#    num_correct, num_samples = 0, len(loader.dataset)
#    for x, y in loader:
#        x_var = to_var(x, volatile=True)
#        scores = model(x_var)
#        _, preds = scores.data.cpu().max(1)
#        num_correct += (preds == y).sum()

#    acc = float(num_correct) / num_samples

#    print('Test accuracy: {:.2f}% ({}/{})'.format(
#        100.*acc,
#        num_correct,
#        num_samples,
#        ))

#    return acc


#def to_var(x, requires_grad=False, volatile=False):
#    """
#    Varialbe type that automatically choose cpu or cuda
#    """
#    if torch.cuda.is_available():
#        x = x.cuda()
#    return Variable(x, requires_grad=requires_grad, volatile=volatile)


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
# Hyper Parameters
param = {
    'batch_size': 128,
    'test_batch_size': 100,
    'num_epochs': 50,
    'learning_rate': 0.001,
    'weight_decay': 5e-4,
}

# MNIST dataset
#train_dataset = torchvision.datasets.MNIST(root='./mnist',
#                                           train=True,
#                                           transform=transforms.ToTensor(),
#                                           download=True)

#test_dataset = torchvision.datasets.MNIST(root='./mnist',
#                                          train=False,
#                                          transform=transforms.ToTensor())

# Data loader
#loader_train = torch.utils.data.DataLoader(dataset=train_dataset,
#                                           batch_size=param['batch_size'],
#                                           shuffle=True)

#loader_test = torch.utils.data.DataLoader(dataset=test_dataset,
#                                          batch_size=param['batch_size'],
#                                          shuffle=False)

# Fully connected neural network with one hidden layer
class MLP1(nn.Module):
    def __init__(self):
        super(MLP1, self).__init__()
        self.linear1 = nn.Linear(28*28, 512)
        self.relu1 = nn.ReLU(inplace=True)
        self.linear2 = nn.Linear(512, 512)
        self.relu2 = nn.ReLU(inplace=True)
        self.linear3 = nn.Linear(512, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out=self.linear1(out)
        out = self.relu1(out)
        out=self.linear2(out)
        out = self.relu2(out)
        out = self.linear3(out)
        return out




#net = MLP1()


# Retraining
#criterion = nn.CrossEntropyLoss()
#optimizer = torch.optim.RMSprop(net.parameters())#, lr=param['learning_rate'], weight_decay=param['weight_decay'])



#train(net, criterion, optimizer, param, loader_train)
#test(net, loader_test)


# Save and load the entire model
#torch.save(net.state_dict(), './no_reg.pkl')

noreg = MLP1()
noreg.load_state_dict(torch.load('./no_reg.pkl', map_location='cpu'))
#test(test_model, loader_test)

reg = MLP1()
reg.load_state_dict(torch.load('./reg.pkl', map_location='cpu'))

import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gaussian_kde
import seaborn as sns
import statistics

parameters1=[]
for name,param in noreg.named_parameters():
    if 'weight' in name:
        for i in range(len(param[0])):
            parameters1.append(param.data[0][i].item())

parameters2=[]
for name,param in reg.named_parameters():
    if 'weight' in name:
        for i in range(len(param[0])):
            parameters2.append(param.data[0][i].item())

#with open('./noreg.txt', 'w') as f:
#    for item in parameters:
#        f.write("%s\n" % item)

def Average(lst): 
    return sum(lst) / len(lst) 

#print(parameters)
print("Std Dev of No Reg: ", statistics.stdev(parameters1))
print("Mean of No Reg: ", Average(parameters1) )
print("Std Dev of Reg: ", statistics.stdev(parameters2))
print("Mean of Reg: ", Average(parameters2) )
#sns.distplot(np.array(parameters),hist=False)
#plt.show()
import matplotlib as mpl
mpl.use('pdf')

plt.rc('font', family='serif')
plt.rc('text', usetex=True)
#plt.rc('xtick', labelsize=8)
#plt.rc('ytick', labelsize=8)
plt.rc('axes', labelsize=8)

fig = plt.figure(figsize=(4, 3))
ax = fig.add_subplot(1, 1, 1)
fig.subplots_adjust(left=.15, bottom=.16, right=.89, top=.87)


density = gaussian_kde(parameters1)
xs = np.linspace(-0.4,0.4,400)
density.covariance_factor = lambda : .25
density._compute_covariance()
ax.plot(xs,density(xs),color = 'black', ls='dashed',label='Without Regularization')

density = gaussian_kde(parameters2)
xs = np.linspace(-0.4,0.4,400)
density.covariance_factor = lambda : .25
density._compute_covariance()
ax.plot(xs,density(xs), color = 'black', ls='solid', label='With Regularization')

#plt.hist(parameters)
#plt.show()
#ax.grid(linestyle=':', linewidth=0.1)
ax.set_yticklabels([])
#ax.set_xticklabels([])
plt.tick_params(
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off
ax.set_yticklabels(ax.get_yticklabels(), visible=False)
plt.xlabel(r'\textsc{Parameters}')
plt.ylabel(r'\textsc{Frequency}')
#plt.title(r'\textsc{Without Regularization}',fontsize=13)
ax.legend(loc='best',fontsize=6)
#width = 3.487
#height = width / 1.618
#fig.set_size_inches(width, height)
plt.show()
fig.savefig('weight_distribution.pdf')


##No reg Test accuracy: 96.97% (9697/10000)
#0.4524047350059467


#REG: Test accuracy: 98.03% (9803/10000)
#0.0477219502758249
