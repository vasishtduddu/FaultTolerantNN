Starting epoch 1 / 75
t = 100, loss = 0.63669664
t = 200, loss = 0.54508787
t = 300, loss = 0.52329671
t = 400, loss = 0.48037565
Training Accuracy

/usr/local/lib/python3.6/dist-packages/ipykernel_launcher.py:7: UserWarning: volatile was removed and now has no effect. Use `with torch.no_grad():` instead.
  import sys

Accuracy: 86.70% (52022/60000)
Testing Accuracy
Accuracy: 85.15% (8515/10000)
Starting epoch 2 / 75
t = 100, loss = 0.36691442
t = 200, loss = 0.35466918
t = 300, loss = 0.39204079
t = 400, loss = 0.48438829
Training Accuracy
Accuracy: 87.87% (52723/60000)
Testing Accuracy
Accuracy: 86.19% (8619/10000)
Starting epoch 3 / 75
t = 100, loss = 0.33893877
t = 200, loss = 0.35771143
t = 300, loss = 0.28769350
t = 400, loss = 0.31130713
Training Accuracy
Accuracy: 88.68% (53207/60000)
Testing Accuracy
Accuracy: 86.65% (8665/10000)
Starting epoch 4 / 75
t = 100, loss = 0.31740904
t = 200, loss = 0.47772178
t = 300, loss = 0.28718787
t = 400, loss = 0.29262078
Training Accuracy
Accuracy: 88.60% (53160/60000)
Testing Accuracy
Accuracy: 86.38% (8638/10000)
Starting epoch 5 / 75
t = 100, loss = 0.16034767
t = 200, loss = 0.20894036
t = 300, loss = 0.37458184
t = 400, loss = 0.32945168
Training Accuracy
Accuracy: 89.59% (53754/60000)
Testing Accuracy
Accuracy: 87.21% (8721/10000)
Starting epoch 6 / 75
t = 100, loss = 0.33325204
t = 200, loss = 0.22721967
t = 300, loss = 0.29695201
t = 400, loss = 0.28313321
Training Accuracy
Accuracy: 90.42% (54253/60000)
Testing Accuracy
Accuracy: 87.68% (8768/10000)
Starting epoch 7 / 75
t = 100, loss = 0.20594998
t = 200, loss = 0.23389471
t = 300, loss = 0.27291667
t = 400, loss = 0.26167211
Training Accuracy
Accuracy: 91.03% (54620/60000)
Testing Accuracy
Accuracy: 88.35% (8835/10000)
Starting epoch 8 / 75
t = 100, loss = 0.23913720
t = 200, loss = 0.27083027
t = 300, loss = 0.13586275
t = 400, loss = 0.20565419
Training Accuracy
Accuracy: 91.28% (54770/60000)
Testing Accuracy
Accuracy: 88.70% (8870/10000)
Starting epoch 9 / 75
t = 100, loss = 0.34296340
t = 200, loss = 0.19490395
t = 300, loss = 0.38838398
t = 400, loss = 0.28599674
Training Accuracy
Accuracy: 91.19% (54712/60000)
Testing Accuracy
Accuracy: 88.11% (8811/10000)
Starting epoch 10 / 75
t = 100, loss = 0.16587207
t = 200, loss = 0.23116182
t = 300, loss = 0.13193019
t = 400, loss = 0.25222823
Training Accuracy
Accuracy: 91.64% (54981/60000)
Testing Accuracy
Accuracy: 88.37% (8837/10000)
Starting epoch 11 / 75
t = 100, loss = 0.23406035
t = 200, loss = 0.18635319
t = 300, loss = 0.17692214
t = 400, loss = 0.16613573
Training Accuracy
Accuracy: 90.60% (54359/60000)
Testing Accuracy
Accuracy: 87.28% (8728/10000)
Starting epoch 12 / 75
t = 100, loss = 0.30544695
t = 200, loss = 0.35712990
t = 300, loss = 0.18526654
t = 400, loss = 0.19591784
Training Accuracy
Accuracy: 92.44% (55463/60000)
Testing Accuracy
Accuracy: 88.77% (8877/10000)
Starting epoch 13 / 75
t = 100, loss = 0.26210073
t = 200, loss = 0.23915409
t = 300, loss = 0.18118839
t = 400, loss = 0.20439748
Training Accuracy
Accuracy: 92.60% (55561/60000)
Testing Accuracy
Accuracy: 89.23% (8923/10000)
Starting epoch 14 / 75
t = 100, loss = 0.25547478
t = 200, loss = 0.18707550
t = 300, loss = 0.25149778
t = 400, loss = 0.17294362
Training Accuracy
Accuracy: 92.77% (55665/60000)
Testing Accuracy
Accuracy: 88.99% (8899/10000)
Starting epoch 15 / 75
t = 100, loss = 0.22308838
t = 200, loss = 0.21378253
t = 300, loss = 0.32146940
t = 400, loss = 0.19661097
Training Accuracy
Accuracy: 92.67% (55602/60000)
Testing Accuracy
Accuracy: 88.88% (8888/10000)
Starting epoch 16 / 75
t = 100, loss = 0.23479193
t = 200, loss = 0.11306241
t = 300, loss = 0.19227329
t = 400, loss = 0.15921487
Training Accuracy
Accuracy: 91.64% (54986/60000)
Testing Accuracy
Accuracy: 87.70% (8770/10000)
Starting epoch 17 / 75
t = 100, loss = 0.15501729
t = 200, loss = 0.19629413
t = 300, loss = 0.18896310
t = 400, loss = 0.33134851
Training Accuracy
Accuracy: 93.12% (55874/60000)
Testing Accuracy
Accuracy: 88.69% (8869/10000)
Starting epoch 18 / 75
t = 100, loss = 0.20596150
t = 200, loss = 0.14297390
t = 300, loss = 0.15165648
t = 400, loss = 0.15975435
Training Accuracy
Accuracy: 93.16% (55897/60000)
Testing Accuracy
Accuracy: 89.05% (8905/10000)
Starting epoch 19 / 75
t = 100, loss = 0.19110703
t = 200, loss = 0.22707939
t = 300, loss = 0.16900082
t = 400, loss = 0.11503981
Training Accuracy
Accuracy: 93.22% (55929/60000)
Testing Accuracy
Accuracy: 89.00% (8900/10000)
Starting epoch 20 / 75
t = 100, loss = 0.13048497
t = 200, loss = 0.13655671
t = 300, loss = 0.21503103
t = 400, loss = 0.16986957
Training Accuracy
Accuracy: 93.68% (56207/60000)
Testing Accuracy
Accuracy: 89.38% (8938/10000)
Starting epoch 21 / 75
t = 100, loss = 0.18732736
t = 200, loss = 0.26395255
t = 300, loss = 0.12354317
t = 400, loss = 0.23476534
Training Accuracy
Accuracy: 93.20% (55919/60000)
Testing Accuracy
Accuracy: 88.31% (8831/10000)
Starting epoch 22 / 75
t = 100, loss = 0.25424919
t = 200, loss = 0.26546416
t = 300, loss = 0.10077550
t = 400, loss = 0.15799919
Training Accuracy
Accuracy: 92.53% (55518/60000)
Testing Accuracy
Accuracy: 87.81% (8781/10000)
Starting epoch 23 / 75
t = 100, loss = 0.19560683
t = 200, loss = 0.24775833
t = 300, loss = 0.19474848
t = 400, loss = 0.15323490
Training Accuracy
Accuracy: 93.64% (56182/60000)
Testing Accuracy
Accuracy: 88.64% (8864/10000)
Starting epoch 24 / 75
t = 100, loss = 0.12313046
t = 200, loss = 0.26506326
t = 300, loss = 0.10632332
t = 400, loss = 0.18358982
Training Accuracy
Accuracy: 94.67% (56801/60000)
Testing Accuracy
Accuracy: 89.50% (8950/10000)
Starting epoch 25 / 75
t = 100, loss = 0.17571133
t = 200, loss = 0.18486774
t = 300, loss = 0.13959651
t = 400, loss = 0.12128210
Training Accuracy
Accuracy: 94.43% (56658/60000)
Testing Accuracy
Accuracy: 88.98% (8898/10000)
Starting epoch 26 / 75
t = 100, loss = 0.14745012
t = 200, loss = 0.10861185
t = 300, loss = 0.20316300
t = 400, loss = 0.16951306
Training Accuracy
Accuracy: 93.93% (56360/60000)
Testing Accuracy
Accuracy: 88.83% (8883/10000)
Starting epoch 27 / 75
t = 100, loss = 0.11386888
t = 200, loss = 0.09918157
t = 300, loss = 0.11608316
t = 400, loss = 0.20468375
Training Accuracy
Accuracy: 94.34% (56603/60000)
Testing Accuracy
Accuracy: 88.71% (8871/10000)
Starting epoch 28 / 75
t = 100, loss = 0.07881887
t = 200, loss = 0.13716803
t = 300, loss = 0.08290674
t = 400, loss = 0.14516573
Training Accuracy
Accuracy: 94.52% (56709/60000)
Testing Accuracy
Accuracy: 88.56% (8856/10000)
Starting epoch 29 / 75
t = 100, loss = 0.18274522
t = 200, loss = 0.08051477
t = 300, loss = 0.15662734
t = 400, loss = 0.17200698
Training Accuracy
Accuracy: 94.31% (56584/60000)
Testing Accuracy
Accuracy: 88.62% (8862/10000)
Starting epoch 30 / 75
t = 100, loss = 0.15034714
t = 200, loss = 0.10186534
t = 300, loss = 0.07705645
t = 400, loss = 0.12797603
Training Accuracy
Accuracy: 94.96% (56978/60000)
Testing Accuracy
Accuracy: 89.20% (8920/10000)
Starting epoch 31 / 75
t = 100, loss = 0.15701249
t = 200, loss = 0.16317101
t = 300, loss = 0.12472029
t = 400, loss = 0.20579220
Training Accuracy
Accuracy: 94.68% (56806/60000)
Testing Accuracy
Accuracy: 89.06% (8906/10000)
Starting epoch 32 / 75
t = 100, loss = 0.18311328
t = 200, loss = 0.14837220
t = 300, loss = 0.16846673
t = 400, loss = 0.16002466
Training Accuracy
Accuracy: 95.44% (57263/60000)
Testing Accuracy
Accuracy: 89.35% (8935/10000)
Starting epoch 33 / 75
t = 100, loss = 0.17572223
t = 200, loss = 0.11862607
t = 300, loss = 0.04833776
t = 400, loss = 0.23163585
Training Accuracy
Accuracy: 95.36% (57218/60000)
Testing Accuracy
Accuracy: 89.36% (8936/10000)
Starting epoch 34 / 75
t = 100, loss = 0.14117663
t = 200, loss = 0.10162228
t = 300, loss = 0.06005611
t = 400, loss = 0.19458672
Training Accuracy
Accuracy: 95.62% (57375/60000)
Testing Accuracy
Accuracy: 89.43% (8943/10000)
Starting epoch 35 / 75
t = 100, loss = 0.10485291
t = 200, loss = 0.16745698
t = 300, loss = 0.12443766
t = 400, loss = 0.14505480
Training Accuracy
Accuracy: 95.29% (57172/60000)
Testing Accuracy
Accuracy: 89.22% (8922/10000)
Starting epoch 36 / 75
t = 100, loss = 0.12201649
t = 200, loss = 0.09360652
t = 300, loss = 0.10755630
t = 400, loss = 0.16214164
Training Accuracy
Accuracy: 95.71% (57427/60000)
Testing Accuracy
Accuracy: 89.53% (8953/10000)
Starting epoch 37 / 75
t = 100, loss = 0.16816995
t = 200, loss = 0.14468335
t = 300, loss = 0.07675640
t = 400, loss = 0.12733430
Training Accuracy
Accuracy: 95.62% (57372/60000)
Testing Accuracy
Accuracy: 89.42% (8942/10000)
Starting epoch 38 / 75
t = 100, loss = 0.15841742
t = 200, loss = 0.13092527
t = 300, loss = 0.06362578
t = 400, loss = 0.16894734
Training Accuracy
Accuracy: 95.26% (57157/60000)
Testing Accuracy
Accuracy: 89.08% (8908/10000)
Starting epoch 39 / 75
t = 100, loss = 0.13585752
t = 200, loss = 0.11756072
t = 300, loss = 0.09059988
t = 400, loss = 0.13972345
Training Accuracy
Accuracy: 96.07% (57642/60000)
Testing Accuracy
Accuracy: 89.48% (8948/10000)
Starting epoch 40 / 75
t = 100, loss = 0.17705417
t = 200, loss = 0.10397110
t = 300, loss = 0.11100125
t = 400, loss = 0.16373633
Training Accuracy
Accuracy: 95.35% (57209/60000)
Testing Accuracy
Accuracy: 88.97% (8897/10000)
Starting epoch 41 / 75
t = 100, loss = 0.19076213
t = 200, loss = 0.17057689
t = 300, loss = 0.24246523
t = 400, loss = 0.06969941
Training Accuracy
Accuracy: 95.20% (57118/60000)
Testing Accuracy
Accuracy: 88.83% (8883/10000)
Starting epoch 42 / 75
t = 100, loss = 0.13218708
t = 200, loss = 0.22593507
t = 300, loss = 0.16548824
t = 400, loss = 0.10588289
Training Accuracy
Accuracy: 95.31% (57184/60000)
Testing Accuracy
Accuracy: 88.75% (8875/10000)
Starting epoch 43 / 75
t = 100, loss = 0.10082497
t = 200, loss = 0.09532923
t = 300, loss = 0.08178981
t = 400, loss = 0.09179466
Training Accuracy
Accuracy: 95.98% (57591/60000)
Testing Accuracy
Accuracy: 89.15% (8915/10000)
Starting epoch 44 / 75
t = 100, loss = 0.09944323
t = 200, loss = 0.09026489
t = 300, loss = 0.11457147
t = 400, loss = 0.10228403
Training Accuracy
Accuracy: 96.02% (57613/60000)
Testing Accuracy
Accuracy: 89.20% (8920/10000)
Starting epoch 45 / 75
t = 100, loss = 0.10686478
t = 200, loss = 0.05295771
t = 300, loss = 0.12228810
t = 400, loss = 0.10117751
Training Accuracy
Accuracy: 95.75% (57450/60000)
Testing Accuracy
Accuracy: 89.17% (8917/10000)
Starting epoch 46 / 75
t = 100, loss = 0.12363841
t = 200, loss = 0.09611198
t = 300, loss = 0.08649920
t = 400, loss = 0.08233621
Training Accuracy
Accuracy: 96.36% (57813/60000)
Testing Accuracy
Accuracy: 89.08% (8908/10000)
Starting epoch 47 / 75
t = 100, loss = 0.10711624
t = 200, loss = 0.10206127
t = 300, loss = 0.15234028
t = 400, loss = 0.10857110
Training Accuracy
Accuracy: 96.32% (57793/60000)
Testing Accuracy
Accuracy: 89.19% (8919/10000)
Starting epoch 48 / 75
t = 100, loss = 0.12064724
t = 200, loss = 0.09046759
t = 300, loss = 0.06326282
t = 400, loss = 0.14079356
Training Accuracy
Accuracy: 95.79% (57474/60000)
Testing Accuracy
Accuracy: 88.77% (8877/10000)
Starting epoch 49 / 75
t = 100, loss = 0.17746904
t = 200, loss = 0.07995965
t = 300, loss = 0.06631257
t = 400, loss = 0.13067350
Training Accuracy
Accuracy: 96.44% (57863/60000)
Testing Accuracy
Accuracy: 89.36% (8936/10000)
Starting epoch 50 / 75
t = 100, loss = 0.11829795
t = 200, loss = 0.12699059
t = 300, loss = 0.13386956
t = 400, loss = 0.09433585
Training Accuracy
Accuracy: 96.09% (57654/60000)
Testing Accuracy
Accuracy: 88.99% (8899/10000)
Starting epoch 51 / 75
t = 100, loss = 0.07404878
t = 200, loss = 0.12170191
t = 300, loss = 0.09936801
t = 400, loss = 0.10159799
Training Accuracy
Accuracy: 96.37% (57823/60000)
Testing Accuracy
Accuracy: 89.00% (8900/10000)
Starting epoch 52 / 75
t = 100, loss = 0.10267238
t = 200, loss = 0.05456051
t = 300, loss = 0.15263756
t = 400, loss = 0.04537686
Training Accuracy
Accuracy: 96.49% (57896/60000)
Testing Accuracy
Accuracy: 89.43% (8943/10000)
Starting epoch 53 / 75
t = 100, loss = 0.08888966
t = 200, loss = 0.04037553
t = 300, loss = 0.12977001
t = 400, loss = 0.11710732
Training Accuracy
Accuracy: 95.90% (57538/60000)
Testing Accuracy
Accuracy: 88.95% (8895/10000)
Starting epoch 54 / 75
t = 100, loss = 0.09693515
t = 200, loss = 0.06940615
t = 300, loss = 0.06783818
t = 400, loss = 0.10398944
Training Accuracy
Accuracy: 96.79% (58072/60000)
Testing Accuracy
Accuracy: 89.38% (8938/10000)
Starting epoch 55 / 75
t = 100, loss = 0.13264087
t = 200, loss = 0.13779198
t = 300, loss = 0.12209075
t = 400, loss = 0.14452286
Training Accuracy
Accuracy: 95.83% (57499/60000)
Testing Accuracy
Accuracy: 88.67% (8867/10000)
Starting epoch 56 / 75
t = 100, loss = 0.11870362
t = 200, loss = 0.07978944
t = 300, loss = 0.07045852
t = 400, loss = 0.11478836
Training Accuracy
Accuracy: 96.65% (57992/60000)
Testing Accuracy
Accuracy: 88.78% (8878/10000)
Starting epoch 57 / 75
t = 100, loss = 0.12155090
t = 200, loss = 0.16139378
t = 300, loss = 0.06425460
t = 400, loss = 0.06079578
Training Accuracy
Accuracy: 96.81% (58084/60000)
Testing Accuracy
Accuracy: 89.52% (8952/10000)
Starting epoch 58 / 75
t = 100, loss = 0.06060331
t = 200, loss = 0.09920055
t = 300, loss = 0.07546324
t = 400, loss = 0.11421843
Training Accuracy
Accuracy: 96.86% (58118/60000)
Testing Accuracy
Accuracy: 88.97% (8897/10000)
Starting epoch 59 / 75
t = 100, loss = 0.07502738
t = 200, loss = 0.10400624
t = 300, loss = 0.10033333
t = 400, loss = 0.13328320
Training Accuracy
Accuracy: 96.49% (57894/60000)
Testing Accuracy
Accuracy: 89.03% (8903/10000)
Starting epoch 60 / 75
t = 100, loss = 0.09455298
t = 200, loss = 0.13290903
t = 300, loss = 0.06768818
t = 400, loss = 0.03960397
Training Accuracy
Accuracy: 96.24% (57743/60000)
Testing Accuracy
Accuracy: 88.95% (8895/10000)
Starting epoch 61 / 75
t = 100, loss = 0.11794513
t = 200, loss = 0.07262219
t = 300, loss = 0.16714723
t = 400, loss = 0.08485343
Training Accuracy
Accuracy: 96.31% (57789/60000)
Testing Accuracy
Accuracy: 88.70% (8870/10000)
Starting epoch 62 / 75
t = 100, loss = 0.03676927
t = 200, loss = 0.11954085
t = 300, loss = 0.08980960
t = 400, loss = 0.16520363
Training Accuracy
Accuracy: 97.26% (58355/60000)
Testing Accuracy
Accuracy: 89.38% (8938/10000)
Starting epoch 63 / 75
t = 100, loss = 0.06686087
t = 200, loss = 0.06481154
t = 300, loss = 0.11361666
t = 400, loss = 0.05616991
Training Accuracy
Accuracy: 97.11% (58267/60000)
Testing Accuracy
Accuracy: 89.02% (8902/10000)
Starting epoch 64 / 75
t = 100, loss = 0.15740030
t = 200, loss = 0.07617684
t = 300, loss = 0.07188693
t = 400, loss = 0.13044858
Training Accuracy
Accuracy: 97.23% (58337/60000)
Testing Accuracy
Accuracy: 88.97% (8897/10000)
Starting epoch 65 / 75
t = 100, loss = 0.04392627
t = 200, loss = 0.08693258
t = 300, loss = 0.07909724
t = 400, loss = 0.13862579
Training Accuracy
Accuracy: 96.80% (58079/60000)
Testing Accuracy
Accuracy: 89.13% (8913/10000)
Starting epoch 66 / 75
t = 100, loss = 0.11863014
t = 200, loss = 0.07930020
t = 300, loss = 0.09256363
t = 400, loss = 0.08789524
Training Accuracy
Accuracy: 96.79% (58076/60000)
Testing Accuracy
Accuracy: 88.85% (8885/10000)
Starting epoch 67 / 75
t = 100, loss = 0.11679748
t = 200, loss = 0.06626806
t = 300, loss = 0.04905683
t = 400, loss = 0.11364049
Training Accuracy
Accuracy: 96.86% (58113/60000)
Testing Accuracy
Accuracy: 89.15% (8915/10000)
Starting epoch 68 / 75
t = 100, loss = 0.11382237
t = 200, loss = 0.05730898
t = 300, loss = 0.15571243
t = 400, loss = 0.08271881
Training Accuracy
Accuracy: 97.31% (58387/60000)
Testing Accuracy
Accuracy: 89.33% (8933/10000)
Starting epoch 69 / 75
t = 100, loss = 0.07557029
t = 200, loss = 0.04032054
t = 300, loss = 0.18594019
t = 400, loss = 0.15521081
Training Accuracy
Accuracy: 97.39% (58436/60000)
Testing Accuracy
Accuracy: 89.38% (8938/10000)
Starting epoch 70 / 75
t = 100, loss = 0.08131495
t = 200, loss = 0.15456808
t = 300, loss = 0.11080454
t = 400, loss = 0.06695449
Training Accuracy
Accuracy: 97.35% (58412/60000)
Testing Accuracy
Accuracy: 88.99% (8899/10000)
Starting epoch 71 / 75
t = 100, loss = 0.10596097
t = 200, loss = 0.06439010
t = 300, loss = 0.14576285
t = 400, loss = 0.09291107
Training Accuracy
Accuracy: 97.40% (58441/60000)
Testing Accuracy
Accuracy: 89.10% (8910/10000)
Starting epoch 72 / 75
t = 100, loss = 0.05206327
t = 200, loss = 0.08712913
t = 300, loss = 0.04730125
t = 400, loss = 0.11993001
Training Accuracy
Accuracy: 97.29% (58376/60000)
Testing Accuracy
Accuracy: 89.38% (8938/10000)
Starting epoch 73 / 75
t = 100, loss = 0.04085363
t = 200, loss = 0.07286710
t = 300, loss = 0.04188424
t = 400, loss = 0.09099788
Training Accuracy
Accuracy: 97.52% (58511/60000)
Testing Accuracy
Accuracy: 88.87% (8887/10000)
Starting epoch 74 / 75
t = 100, loss = 0.05479699
t = 200, loss = 0.07669044
t = 300, loss = 0.06908479
t = 400, loss = 0.12629922
Training Accuracy
Accuracy: 96.30% (57778/60000)
Testing Accuracy
Accuracy: 89.03% (8903/10000)
Starting epoch 75 / 75
t = 100, loss = 0.06434359
t = 200, loss = 0.04738767
t = 300, loss = 0.06941085
t = 400, loss = 0.07382931
Training Accuracy
Accuracy: 97.16% (58298/60000)
Testing Accuracy
Accuracy: 88.52% (8852/10000)
Accuracy: 88.52% (8852/10000)

0.8852
