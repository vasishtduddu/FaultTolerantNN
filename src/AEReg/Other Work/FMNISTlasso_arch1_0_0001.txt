Starting epoch 1 / 75
t = 100, loss = 3.40680838
t = 200, loss = 3.48507380
t = 300, loss = 3.63196421
t = 400, loss = 3.48504806
Training Accuracy

/usr/local/lib/python3.6/dist-packages/ipykernel_launcher.py:7: UserWarning: volatile was removed and now has no effect. Use `with torch.no_grad():` instead.
  import sys

Accuracy: 86.37% (51820/60000)
Testing Accuracy
Accuracy: 84.88% (8488/10000)
Starting epoch 2 / 75
t = 100, loss = 3.66334391
t = 200, loss = 3.83275199
t = 300, loss = 3.84503174
t = 400, loss = 3.89970660
Training Accuracy
Accuracy: 87.78% (52666/60000)
Testing Accuracy
Accuracy: 85.84% (8584/10000)
Starting epoch 3 / 75
t = 100, loss = 4.02317905
t = 200, loss = 4.02607918
t = 300, loss = 4.11082125
t = 400, loss = 4.14725256
Training Accuracy
Accuracy: 88.47% (53082/60000)
Testing Accuracy
Accuracy: 86.51% (8651/10000)
Starting epoch 4 / 75
t = 100, loss = 4.48444223
t = 200, loss = 4.17317867
t = 300, loss = 4.36390781
t = 400, loss = 4.41399050
Training Accuracy
Accuracy: 89.35% (53613/60000)
Testing Accuracy
Accuracy: 87.02% (8702/10000)
Starting epoch 5 / 75
t = 100, loss = 4.37967539
t = 200, loss = 4.46986151
t = 300, loss = 4.70791721
t = 400, loss = 4.59925699
Training Accuracy
Accuracy: 90.53% (54317/60000)
Testing Accuracy
Accuracy: 88.13% (8813/10000)
Starting epoch 6 / 75
t = 100, loss = 4.69082546
t = 200, loss = 4.75546789
t = 300, loss = 4.78117180
t = 400, loss = 4.87276936
Training Accuracy
Accuracy: 90.11% (54068/60000)
Testing Accuracy
Accuracy: 87.41% (8741/10000)
Starting epoch 7 / 75
t = 100, loss = 5.02581978
t = 200, loss = 5.06484127
t = 300, loss = 4.98996401
t = 400, loss = 5.01992607
Training Accuracy
Accuracy: 91.05% (54633/60000)
Testing Accuracy
Accuracy: 88.25% (8825/10000)
Starting epoch 8 / 75
t = 100, loss = 5.16924572
t = 200, loss = 5.21852398
t = 300, loss = 5.16907883
t = 400, loss = 5.36676884
Training Accuracy
Accuracy: 91.83% (55096/60000)
Testing Accuracy
Accuracy: 88.71% (8871/10000)
Starting epoch 9 / 75
t = 100, loss = 5.36387444
t = 200, loss = 5.46522522
t = 300, loss = 5.51137400
t = 400, loss = 5.53836155
Training Accuracy
Accuracy: 91.52% (54911/60000)
Testing Accuracy
Accuracy: 88.29% (8829/10000)
Starting epoch 10 / 75
t = 100, loss = 5.68388176
t = 200, loss = 5.67944813
t = 300, loss = 5.69025803
t = 400, loss = 5.72113752
Training Accuracy
Accuracy: 91.74% (55044/60000)
Testing Accuracy
Accuracy: 88.58% (8858/10000)
Starting epoch 11 / 75
t = 100, loss = 5.87955093
t = 200, loss = 6.00261307
t = 300, loss = 6.01516294
t = 400, loss = 6.09550714
Training Accuracy
Accuracy: 92.11% (55267/60000)
Testing Accuracy
Accuracy: 88.34% (8834/10000)
Starting epoch 12 / 75
t = 100, loss = 6.07521582
t = 200, loss = 6.13486576
t = 300, loss = 6.25062943
t = 400, loss = 6.33245277
Training Accuracy
Accuracy: 92.78% (55667/60000)
Testing Accuracy
Accuracy: 89.06% (8906/10000)
Starting epoch 13 / 75
t = 100, loss = 6.30129623
t = 200, loss = 6.38152409
t = 300, loss = 6.43310404
t = 400, loss = 6.42566919
Training Accuracy
Accuracy: 93.14% (55882/60000)
Testing Accuracy
Accuracy: 89.01% (8901/10000)
Starting epoch 14 / 75
t = 100, loss = 6.52082729
t = 200, loss = 6.49709368
t = 300, loss = 6.67834711
t = 400, loss = 6.69129944
Training Accuracy
Accuracy: 93.14% (55885/60000)
Testing Accuracy
Accuracy: 88.89% (8889/10000)
Starting epoch 15 / 75
t = 100, loss = 6.80911160
t = 200, loss = 6.82378864
t = 300, loss = 6.84405899
t = 400, loss = 6.89192724
Training Accuracy
Accuracy: 93.86% (56319/60000)
Testing Accuracy
Accuracy: 89.20% (8920/10000)
Starting epoch 16 / 75
t = 100, loss = 7.02951717
t = 200, loss = 7.03117371
t = 300, loss = 7.14159489
t = 400, loss = 7.11530972
Training Accuracy
Accuracy: 94.23% (56539/60000)
Testing Accuracy
Accuracy: 89.52% (8952/10000)
Starting epoch 17 / 75
t = 100, loss = 7.19912434
t = 200, loss = 7.16684055
t = 300, loss = 7.22249651
t = 400, loss = 7.24438763
Training Accuracy
Accuracy: 94.12% (56473/60000)
Testing Accuracy
Accuracy: 89.33% (8933/10000)
Starting epoch 18 / 75
t = 100, loss = 7.36090612
t = 200, loss = 7.40887213
t = 300, loss = 7.53661489
t = 400, loss = 7.51953125
Training Accuracy
Accuracy: 94.50% (56699/60000)
Testing Accuracy
Accuracy: 89.21% (8921/10000)
Starting epoch 19 / 75
t = 100, loss = 7.57640934
t = 200, loss = 7.62321329
t = 300, loss = 7.68951178
t = 400, loss = 7.70495796
Training Accuracy
Accuracy: 93.43% (56056/60000)
Testing Accuracy
Accuracy: 88.35% (8835/10000)
Starting epoch 20 / 75
t = 100, loss = 7.78889036
t = 200, loss = 7.87023211
t = 300, loss = 7.79162645
t = 400, loss = 7.89075279
Training Accuracy
Accuracy: 94.38% (56626/60000)
Testing Accuracy
Accuracy: 88.91% (8891/10000)
Starting epoch 21 / 75
t = 100, loss = 7.99618864
t = 200, loss = 7.97589731
t = 300, loss = 8.03090668
t = 400, loss = 8.13600922
Training Accuracy
Accuracy: 95.25% (57153/60000)
Testing Accuracy
Accuracy: 89.83% (8983/10000)
Starting epoch 22 / 75
t = 100, loss = 8.19118214
t = 200, loss = 8.23196983
t = 300, loss = 8.23217583
t = 400, loss = 8.34892368
Training Accuracy
Accuracy: 94.72% (56833/60000)
Testing Accuracy
Accuracy: 89.21% (8921/10000)
Starting epoch 23 / 75
t = 100, loss = 8.36779594
t = 200, loss = 8.45565414
t = 300, loss = 8.46700382
t = 400, loss = 8.48091698
Training Accuracy
Accuracy: 95.62% (57373/60000)
Testing Accuracy
Accuracy: 89.46% (8946/10000)
Starting epoch 24 / 75
t = 100, loss = 8.50798988
t = 200, loss = 8.51085186
t = 300, loss = 8.61053562
t = 400, loss = 8.65663433
Training Accuracy
Accuracy: 95.44% (57261/60000)
Testing Accuracy
Accuracy: 89.35% (8935/10000)
Starting epoch 25 / 75
t = 100, loss = 8.70919609
t = 200, loss = 8.69910622
t = 300, loss = 8.76889896
t = 400, loss = 8.74237442
Training Accuracy
Accuracy: 95.12% (57070/60000)
Testing Accuracy
Accuracy: 88.92% (8892/10000)
Starting epoch 26 / 75
t = 100, loss = 8.81243324
t = 200, loss = 8.83697510
t = 300, loss = 8.95960712
t = 400, loss = 8.95994473
Training Accuracy
Accuracy: 95.97% (57583/60000)
Testing Accuracy
Accuracy: 89.50% (8950/10000)
Starting epoch 27 / 75
t = 100, loss = 9.01281452
t = 200, loss = 9.06785774
t = 300, loss = 9.17665386
t = 400, loss = 9.25236225
Training Accuracy
Accuracy: 96.26% (57755/60000)
Testing Accuracy
Accuracy: 89.92% (8992/10000)
Starting epoch 28 / 75
t = 100, loss = 9.18256569
t = 200, loss = 9.19389820
t = 300, loss = 9.22037697
t = 400, loss = 9.33355618
Training Accuracy
Accuracy: 96.43% (57858/60000)
Testing Accuracy
Accuracy: 89.44% (8944/10000)
Starting epoch 29 / 75
t = 100, loss = 9.30668163
t = 200, loss = 9.33129215
t = 300, loss = 9.46863747
t = 400, loss = 9.48455048
Training Accuracy
Accuracy: 95.38% (57230/60000)
Testing Accuracy
Accuracy: 88.91% (8891/10000)
Starting epoch 30 / 75
t = 100, loss = 9.49920559
t = 200, loss = 9.50435352
t = 300, loss = 9.62599468
t = 400, loss = 9.59096146
Training Accuracy
Accuracy: 96.55% (57927/60000)
Testing Accuracy
Accuracy: 89.50% (8950/10000)
Starting epoch 31 / 75
t = 100, loss = 9.64211464
t = 200, loss = 9.63209057
t = 300, loss = 9.74152756
t = 400, loss = 9.72443962
Training Accuracy
Accuracy: 96.49% (57894/60000)
Testing Accuracy
Accuracy: 89.59% (8959/10000)
Starting epoch 32 / 75
t = 100, loss = 9.77265644
t = 200, loss = 9.76743317
t = 300, loss = 9.80402088
t = 400, loss = 10.08616734
Training Accuracy
Accuracy: 96.37% (57820/60000)
Testing Accuracy
Accuracy: 89.30% (8930/10000)
Starting epoch 33 / 75
t = 100, loss = 9.89479733
t = 200, loss = 9.93012524
t = 300, loss = 9.95621109
t = 400, loss = 9.98425293
Training Accuracy
Accuracy: 96.98% (58191/60000)
Testing Accuracy
Accuracy: 89.24% (8924/10000)
Starting epoch 34 / 75
t = 100, loss = 10.01370049
t = 200, loss = 10.09119892
t = 300, loss = 10.07655811
t = 400, loss = 10.15629959
Training Accuracy
Accuracy: 97.35% (58409/60000)
Testing Accuracy
Accuracy: 89.50% (8950/10000)
Starting epoch 35 / 75
t = 100, loss = 10.13680744
t = 200, loss = 10.22953224
t = 300, loss = 10.26984692
t = 400, loss = 10.26431561
Training Accuracy
Accuracy: 97.03% (58216/60000)
Testing Accuracy
Accuracy: 89.02% (8902/10000)
Starting epoch 36 / 75
t = 100, loss = 10.28151417
t = 200, loss = 10.35537910
t = 300, loss = 10.30492783
t = 400, loss = 10.35624313
Training Accuracy
Accuracy: 96.20% (57720/60000)
Testing Accuracy
Accuracy: 89.16% (8916/10000)
Starting epoch 37 / 75
t = 100, loss = 10.46423912
t = 200, loss = 10.51698208
t = 300, loss = 10.48515987
t = 400, loss = 10.49951267
Training Accuracy
Accuracy: 97.47% (58480/60000)
Testing Accuracy
Accuracy: 89.64% (8964/10000)
Starting epoch 38 / 75
t = 100, loss = 10.60645390
t = 200, loss = 10.56277370
t = 300, loss = 10.62779427
t = 400, loss = 10.62127781
Training Accuracy
Accuracy: 96.92% (58155/60000)
Testing Accuracy
Accuracy: 89.02% (8902/10000)
Starting epoch 39 / 75
t = 100, loss = 10.68732452
t = 200, loss = 10.70765400
t = 300, loss = 10.78526211
t = 400, loss = 10.79205418
Training Accuracy
Accuracy: 97.22% (58332/60000)
Testing Accuracy
Accuracy: 89.23% (8923/10000)
Starting epoch 40 / 75
t = 100, loss = 10.84824848
t = 200, loss = 10.85665321
t = 300, loss = 10.82870483
t = 400, loss = 10.89987659
Training Accuracy
Accuracy: 97.07% (58243/60000)
Testing Accuracy
Accuracy: 89.03% (8903/10000)
Starting epoch 41 / 75
t = 100, loss = 10.91143417
t = 200, loss = 10.97990894
t = 300, loss = 10.95495510
t = 400, loss = 11.08087254
Training Accuracy
Accuracy: 96.65% (57992/60000)
Testing Accuracy
Accuracy: 89.19% (8919/10000)
Starting epoch 42 / 75
t = 100, loss = 11.04292870
t = 200, loss = 11.05503368
t = 300, loss = 11.07815742
t = 400, loss = 11.14757156
Training Accuracy
Accuracy: 97.30% (58381/60000)
Testing Accuracy
Accuracy: 89.01% (8901/10000)
Starting epoch 43 / 75
t = 100, loss = 11.16210175
t = 200, loss = 11.24062634
t = 300, loss = 11.23478985
t = 400, loss = 11.24671268
Training Accuracy
Accuracy: 98.15% (58888/60000)
Testing Accuracy
Accuracy: 89.61% (8961/10000)
Starting epoch 44 / 75
t = 100, loss = 11.22976398
t = 200, loss = 11.24781609
t = 300, loss = 11.29193687
t = 400, loss = 11.38586712
Training Accuracy
Accuracy: 97.67% (58603/60000)
Testing Accuracy
Accuracy: 89.03% (8903/10000)
Starting epoch 45 / 75
t = 100, loss = 11.38022232
t = 200, loss = 11.48371220
t = 300, loss = 11.42076492
t = 400, loss = 11.45350838
Training Accuracy
Accuracy: 97.90% (58740/60000)
Testing Accuracy
Accuracy: 89.36% (8936/10000)
Starting epoch 46 / 75
t = 100, loss = 11.71814728
t = 200, loss = 11.54588509
t = 300, loss = 11.56782150
t = 400, loss = 11.59123039
Training Accuracy
Accuracy: 98.31% (58986/60000)
Testing Accuracy
Accuracy: 89.99% (8999/10000)
Starting epoch 47 / 75
t = 100, loss = 11.63081551
t = 200, loss = 11.69601059
t = 300, loss = 11.64823914
t = 400, loss = 11.69252586
Training Accuracy
Accuracy: 98.44% (59062/60000)
Testing Accuracy
Accuracy: 89.96% (8996/10000)
Starting epoch 48 / 75
t = 100, loss = 11.71649837
t = 200, loss = 11.72712517
t = 300, loss = 11.81411552
t = 400, loss = 11.76466751
Training Accuracy
Accuracy: 97.84% (58705/60000)
Testing Accuracy
Accuracy: 89.36% (8936/10000)
Starting epoch 49 / 75
t = 100, loss = 11.82007599
t = 200, loss = 11.89503670
t = 300, loss = 11.86658859
t = 400, loss = 11.91670895
Training Accuracy
Accuracy: 98.36% (59018/60000)
Testing Accuracy
Accuracy: 89.72% (8972/10000)
Starting epoch 50 / 75
t = 100, loss = 11.89364815
t = 200, loss = 11.91881275
t = 300, loss = 12.00951195
t = 400, loss = 12.02911091
Training Accuracy
Accuracy: 98.25% (58947/60000)
Testing Accuracy
Accuracy: 89.72% (8972/10000)
Starting epoch 51 / 75
t = 100, loss = 12.00023079
t = 200, loss = 12.15634727
t = 300, loss = 12.05036926
t = 400, loss = 12.12525463
Training Accuracy
Accuracy: 98.41% (59043/60000)
Testing Accuracy
Accuracy: 89.69% (8969/10000)
Starting epoch 52 / 75
t = 100, loss = 12.11358452
t = 200, loss = 12.18105698
t = 300, loss = 12.21201992
t = 400, loss = 12.21176720
Training Accuracy
Accuracy: 98.04% (58823/60000)
Testing Accuracy
Accuracy: 89.09% (8909/10000)
Starting epoch 53 / 75
t = 100, loss = 12.23058796
t = 200, loss = 12.21455097
t = 300, loss = 12.26138973
t = 400, loss = 12.28937531
Training Accuracy
Accuracy: 98.16% (58897/60000)
Testing Accuracy
Accuracy: 89.39% (8939/10000)
Starting epoch 54 / 75
t = 100, loss = 12.34462166
t = 200, loss = 12.37561703
t = 300, loss = 12.40548134
t = 400, loss = 12.46604824
Training Accuracy
Accuracy: 98.42% (59052/60000)
Testing Accuracy
Accuracy: 88.96% (8896/10000)
Starting epoch 55 / 75
t = 100, loss = 12.48073387
t = 200, loss = 12.44945526
t = 300, loss = 12.46418285
t = 400, loss = 12.55445004
Training Accuracy

