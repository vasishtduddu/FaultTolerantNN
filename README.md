# Adversarial Fault Tolerance Training for Deep Neural Networks

The repository includes the code for the paper titled "Adversarial Fault Tolerant Training for Deep Neural Networks" [Paper](https://arxiv.org/abs/1907.03103).
The Pre-Trained Model Weights for the models used for different experiments are available at: https://gitlab.com/vduddu/PreTrainedModels.
All code is present in .ipynb files for easy reproducibility.

All the code is available in the "/Code" folder.

- "/Code/AEReg": The unsupervised pre-training followed by supervised training apporach is present in this folder. This also includes the code for injecting the faults for different datasets and architecture.

- "/Code/Distribution": The code for distribution of parameter values for the regularized and unregularized models.

- "/Code/FilterFaults" and "/Code/WeightFaults" include the code for inserting faults into different Neural Networks trained with different training algorithms.

- "/Code/Loss": This includes the loss for Generative and Discriminator model loss. This includes the min loss, max loss (adversarial loss) and reconstruction loss.